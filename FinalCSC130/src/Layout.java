/*	
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 * Copyright (c) 2009, Mikio L. Braun. All rights reserved.
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.jblas.DoubleMatrix;
import org.jblas.SimpleBlas;
import org.jblas.Solve;

public class Layout extends JFrame {
    private JPanel pnlOne, pnlTwo, pnlThree, pnlLayout;
    private JLabel question1, question2, RowLabel[],empty;
    private JButton btnOutput, btnInput;
    private JTextField answer1, answer2, PReturn, PVariance;
	private JTextField OutputWeight[], OutputInvValue[];
    private JTextField AssetLabel[], ExpectedReturn[], InputField[];
    
    private DoubleMatrix B, A, Aplus, X;
	private static int Size;
	private static double pVar, pReturn;
	DecimalFormat Nformatter = new DecimalFormat("#.##"); //USE: (String) Nformatter.format(????);
	

/**	set name and visible for layout*/
	public Layout(JPanel pnlLayout) {
		this.add(pnlLayout);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Min-Variance Portfolio Constructor");
		setVisible(true);
		pack();
	}

/** set up part one: initial Q&A*/
	public void createPanelOne(final Container Pane) {
		int AnswerField = 10;
		pnlOne = new JPanel();
		pnlOne.setLayout(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();	
		question1 = new JLabel("How many assets do you want to invest? ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		pnlOne.add(question1,c);
		
		answer1 = new JTextField("");
		answer1.setColumns(AnswerField);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		pnlOne.add(answer1,c);
		
		question2 = new JLabel("What is your total investment value? ");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		pnlOne.add(question2,c);
		
		answer2 = new JTextField(); 
		answer2.setColumns(AnswerField);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		pnlOne.add(answer2,c);	

		btnInput = new JButton("Click To Create Input Table");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 2;
		pnlOne.add(btnInput,c);
		Pane.add(pnlOne);
		pack();

		//implement action to button1: show initial input & update Size
		btnInput.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setSize();
        		JPanel frame = new JPanel();
        		frame.add(createPanelTwo());
        		pack();
        		Layout display = new Layout(frame);
            }});
	}
	
	
/** hide initial Q&A panel*/
	public void hidePanelOne() {
		pnlOne.setVisible(false);
	}
	
/** show initial info: number of assets, total investment */	
	public JPanel showPanelOne() {
		pnlThree = new JPanel();
		pnlThree.setLayout(new GridLayout(2,0));
		String nAssets = answer1.getText();
		int invValue = Integer.parseInt(answer2.getText());
		JLabel totalAsset = new JLabel("Number of invested assets: "+nAssets);
		JLabel totalInv = new JLabel("Total investment value: $"+invValue);
		pnlThree.add(totalAsset);
		pnlThree.add(totalInv);
		pack();
	return pnlThree;
	}
	
	public void setSize() {
		this.Size = Integer.parseInt(answer1.getText());
	}

	
	
	/**	set up part two: input & output table*/	
	public JPanel createPanelTwo() {
	    pnlTwo = new JPanel();
	    pnlTwo.setLayout(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	
	    //define column labels @ row 0
	    //enter expected return @ row 1
		AssetLabel = new JTextField[Size];
		ExpectedReturn = new JTextField[Size];
		for (int k=0; k< Size; k++) {
			AssetLabel[k] = new JTextField("Asset " + (k+1));
			AssetLabel[k].setBackground(Color.white);
				ExpectedReturn[k] = new JTextField("E(r"+(k+1)+")");
				ExpectedReturn[k].setBackground(Color.yellow);
			c.fill = GridBagConstraints.HORIZONTAL;
			c.ipady = 20;
			c.gridx = k+1;
				c.gridy = 0;
				pnlTwo.add(AssetLabel[k],c);
			c.gridy = 1;
			pnlTwo.add(ExpectedReturn[k],c);
			}	

		//define row labels
		RowLabel = new JLabel[Size+7];
			String[] RLabel = new String[Size+7];
				RLabel[0] = "Asset Class";
				RLabel[1] = "Expected Return (%)";
				RLabel[Size+3] = "Asset weight (%)";
				RLabel[Size+4] = "Investment value ($)";
				RLabel[Size+5] = "Portfolio return (%)";
				RLabel[Size+6] = "Portfolio variance";
				for (int j=0; j<Size; j++){
					RLabel[j+2] = AssetLabel[j].getText();
					}
			for (int i=0; i< Size+7; i++) {
				RowLabel[i] = new JLabel();
				RowLabel[i].setText(RLabel[i]);
				c.fill = GridBagConstraints.HORIZONTAL;
				c.ipady = 20;
				c.gridx = 0;
				c.gridy = i;
				c.anchor = GridBagConstraints.LINE_START;
				pnlTwo.add(RowLabel[i],c);
				}
					
			//add button2
			btnOutput = new JButton("Click To See Result");
			c.fill = GridBagConstraints.HORIZONTAL;
			c.ipady = 0;
			c.gridx = 0;
			c.gridy = 2+Size;
			pnlTwo.add(btnOutput,c);
			
			//implement action to btnOutput
			btnOutput.addActionListener(new ActionListener(){
	            public void actionPerformed(ActionEvent e){
	        		//update & format PReturn, PVar
	            	showMatrixResult();
	            	PReturn.setText(Nformatter.format(getPReturn()));
	            	PVariance.setText(Nformatter.format(getPVar()));
	            }});
				
		//create input table from row2 & col1
		int counter=-1;
		InputField = new JTextField[Size*Size];
		for (int k=0; k < Size; k++){ 		//column counter
			int r=0;
			for (r=0; r< Size; r++) {		//row counter
			counter++;						//array counter: NS then WE
				if (r>k) {InputField[counter] = new JTextField();}  //NO SHOW!
				else {	//set Input Note
					if (r==k) {InputField[counter] = new JTextField("VAR "+(k+1));}
					else {InputField[counter] = new JTextField("COV "+(k+1)+"-"+(r+1));}
				//locate visible InputField
				InputField[counter].setBackground(Color.WHITE);
				c.fill = GridBagConstraints.HORIZONTAL;
				c.ipady = 20;
				c.gridx = k+1;
				c.gridy = r+2;
			pnlTwo.add(InputField[counter],c);	
		}	}	}

		//create output table start at Col1 & Row(Size+3), output in 2 rows 
		OutputWeight = new JTextField[Size];
		OutputInvValue = new JTextField[Size];
		for (int k=0; k< Size; k++){ 		//column&asset counter
				OutputWeight[k] = new JTextField("Weight "+(k+1));
				OutputWeight[k].setBackground(Color.lightGray);
			OutputInvValue[k] = new JTextField("INV "+(k+1));
			OutputInvValue[k].setBackground(Color.lightGray);
			c.fill = GridBagConstraints.HORIZONTAL;
			c.ipady = 20;
			c.gridx = k+1;
				c.gridy = 3+Size;
				pnlTwo.add(OutputWeight[k],c);
			c.gridy = 4+Size;
			pnlTwo.add(OutputInvValue[k],c);
		}
		
		//show portfolio return & variance in Col1 & Row(Size+5,6)
		PReturn = new JTextField();
		PReturn.setBackground(Color.lightGray);
		c.gridx = 1;
		c.gridy = 5+Size;
		pnlTwo.add(PReturn,c);
		PVariance = new JTextField();
		PVariance.setBackground(Color.lightGray);
		c.gridx = 1;
		c.gridy = 6+Size;
		pnlTwo.add(PVariance,c);
		pack();
	return pnlTwo;
	}
			

	/**Calculate portfolio return*/
	public double getPReturn(){
		for (int k=0; k<Size; k++){
			double[] Return = new double[Size];
			double[] Weight = new double[Size];
			Return[k]= Double.parseDouble(ExpectedReturn[k].getText());
			Weight[k]= Double.parseDouble(OutputWeight[k].getText());
			pReturn+= Return[k] * Weight[k]/100;  //unit=% 
		}
	return pReturn;
	}
	

	/**Calculate portfolio variance*/
	public  double getPVar(){
		//create inverse matrix of X = xInverse
		for (int k=0; k<Size; k++){
			DoubleMatrix xInverse = new DoubleMatrix(1,Size);
			for (k=0;k<Size;k++){
				double weight = X.get(k,0);
				xInverse.put(0,k, weight);
			}		 
			System.out.println("Inverse Matrix of X(weight)");//CHECK
			xInverse.print();//CHECK
		//multiply Matrix A(VAR, COV) with Matrix X(weight) = AX
		DoubleMatrix AX = new DoubleMatrix(Size,1);
			AX = A.mmul(X);
			System.out.println("Matrix A*X = Matrix A(VAR, COV) with Matrix X(weight)");//CHECK
			AX.print();//CHECK
		//calculate portfolio variance = xInverse dot AX
		pVar = xInverse.dot(AX);
		System.out.println("scalar pVAR "+pVar); ///CHECK
		}
	return pVar;
	}
		
	/**Show result to outputField*/
	public void showMatrixResult() {
		MatrixWeight();
		System.out.println("Matrix X includes Asset weights only ");//CHECK
		X.print();
			for (int k=0; k<Size; k++) { //Asset/column index
				//Get Weight from Matrix X
				double weight = X.get(k,0);
			//Print weight result from MatrixX to Asset Weight
			OutputWeight[k].setText(Nformatter.format(100*weight));	//convert to %
			//Calculate Inv Amount for each asset
			double invDist =  weight*Double.parseDouble(answer2.getText());
			OutputInvValue[k].setText(Nformatter.format(invDist));		
			}
	}
	

	/**Set up Matrix X, including asset	weights
	 * Solve X s.t. Aplus([A(VAR,COV),lambda, weight] * XweightPlus(X,lambda) = B(0,1)
	 */ 
	public void MatrixWeight(){
		this.X = new DoubleMatrix(Size,1);
		MatrixA();
		ColumnVectorB();
		DoubleMatrix XweightPlus = Solve.solve(Aplus,B);	//A*(Xweight,1)=B
		System.out.println("SOLVE Matrix XweightPlus=[Aplus'][B]");//CHECK
		XweightPlus.print();//CHECK
		for (int i=0; i<Size; i++){			//i=row index
			double xWeight = XweightPlus.get(i,0);
			System.out.println(" SOLVE Weight "+i+" "+xWeight);//CHECK
			this.X.put(i,0, xWeight);}
	}
	
	
	/**Set up Matrix B	*/
	public void ColumnVectorB(){
		this.B = new DoubleMatrix(Size+1,1);
		for (int i=0; i<Size; i++){			//i=row index
			this.B.put(i,0, 0);}						
			this.B.put(Size,0, 1);
		System.out.println("Matrix B plus Lambda ");//CHECK
		B.print();//CHECK
	}

	
	/**Set up Matrix A */
	public void MatrixA(){
		//DoubleMatrix(r,k,value). 
		this.A = new DoubleMatrix(Size,Size);
		this.Aplus = new DoubleMatrix(Size+1,Size+1);
		int counter=-1, i =0;	
		for (int k=0; k<Size; k++){		//k=column or asset index
		for (int r=0; r<Size; r++) {	//r=row index	 	
			counter++;						//counter MatrixA index
			double[] readInput = new double[Size*Size];	//read input covariance
				if(k==r){				//ASSET VARIANCE
					i=r+Size*k;			//inputField index
					readInput[counter] = 2*Double.parseDouble(InputField[i].getText()); //diagonal elements
				}
				else{					//ASSET COVARIANCE
					if (k>r) {i=r+Size*k;}		//counter InputField, lower triangle				
					else {i=k+Size*r;}			//counter InputField, upper triangle				
					readInput[counter] = 2*Double.parseDouble(InputField[i].getText()); //triangular elements		
				}
				this.A.put(r, k, readInput[counter]);  	//set AdjA = var & cov
				Aplus.put(r, k, readInput[counter]);  
		}		
				//set last row in MatrixA = 1
				Aplus.put(Size,k, (int) 1); //last row, k=column index 
				
				//set last column in MatrixA = 1
				Aplus.put(k,Size, (int) 1); //last column, k=row index 
		}		
				Aplus.put(Size,Size, (int) 0); 
		System.out.println("Matrix Aplus includes COV, VAR, Lambda");
		Aplus.print();
		System.out.println("Matrix A includes COV, VAR only");
		A.print();
	}

}