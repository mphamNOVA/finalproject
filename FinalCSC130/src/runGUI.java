import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.*;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;


public class runGUI {

	public static void main(String[] args) {
		//createAndShowGUI();
		JPanel frame = new JPanel();
		Layout display = new Layout(frame);
		frame.setSize(new Dimension());
	
		//Component Panels - show initial Q&A
		display.createPanelOne(frame);
	}
}
